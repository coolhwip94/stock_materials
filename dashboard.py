import ipywidgets as widgets
import json
import requests
from IPython.display import display
from common import td_api_key, yahoo_api_key
from math import floor
from api_funcs import (get_ticker_quotes, get_yahoo_ticker_quotes,
                       get_yahoo_ticker_statistics, gather_report,
                       calculate_position, risk_reward_report)


class Dashboard:

    def __init__(self):

        self.build_ui()

    def build_ui(self):

        self.buy_price = widgets.FloatText(description="Buy Price")
        self.buy_slider = widgets.FloatSlider(step=0.01,
                                              readout=True,
                                              readout_format='.1f')

        self.sell_price = widgets.FloatText(description="Sell Price")
        self.sell_slider = widgets.FloatSlider(step=0.01,
                                               readout=True,
                                               readout_format='.1f')

        self.stop_price = widgets.FloatText(description="Stop Price")
        self.stop_slider = widgets.FloatSlider(step=0.01,
                                               readout=True,
                                               readout_format='.1f')

        self.funds = widgets.FloatText(description="Funds")
        self.funds_slider = widgets.FloatSlider(step=0.01,
                                                readout=True,
                                                max=1000,
                                                readout_format='.1f')

        self.output = widgets.Textarea(
            value='Press Submit to Calculate',
            placeholder='Type something',
            description='Risk/Reward:',
            disabled=False,
            layout={'height': '80%', 'width': '60%'},)

        self.submit_button = widgets.Button(
            description='Submit',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check',)

        widgets.jslink((self.buy_price, 'value'), (self.buy_slider, 'value'))
        widgets.jslink((self.sell_price, 'value'), (self.sell_slider, 'value'))
        widgets.jslink((self.stop_price, 'value'), (self.stop_slider, 'value'))
        widgets.jslink((self.funds, 'value'),
                       (self.funds_slider, 'value'))
        self.ticker = widgets.Text(
            value='AAPL',
            placeholder='enter a ticker',
            description='Ticker:',
            disabled=False
        )

        self.ticker_submit_button = widgets.Button(
            description='Get Ticker',
            disabled=False,
            button_style='',  # 'success', 'info', 'warning', 'danger' or ''
            tooltip='Click me',
            icon='check')

        self.ticker_output = widgets.Textarea(
            value='Press Submit retrieve data',
            placeholder='Type something',
            description='Details:',
            disabled=False,
            layout={'height': '80%'},)

        self.left_box = widgets.VBox([self.buy_price, self.buy_slider,
                                      self.sell_price, self.sell_slider,
                                      self.ticker,
                                      self.ticker_output, self.ticker_submit_button],
                                     layout={'height': '550px'})

        self.right_box = widgets.VBox([self.stop_price, self.stop_slider, self.funds, self.funds_slider, self.output, self.submit_button, ],
                                      layout={'height': '550px',
                                              'width': '550px'})

        display(widgets.HBox(
            [self.left_box, self.right_box], layout={'height': '500%'}))

        self.submit_button.on_click(
            lambda x: self.submit_button_click(self.buy_price, self.sell_price, self.funds, self.stop_price, self.output))

        self.ticker_submit_button.on_click(
            lambda x: self.ticker_button_click(ticker=self.ticker, yahoo_api_key=yahoo_api_key, td_api_key=td_api_key))

    def submit_button_click(self, buy_price, sell_price, funds, stop_price, output):
        risk_reward_query = {
            'buy_price': buy_price.value,
            'sell_price': sell_price.value,
            'stop_loss': stop_price.value,
            'funds': funds.value,
        }
        report = risk_reward_report(**risk_reward_query)

        # output the report
        self.output.value = json.dumps(
            report, indent=4)

        # output the buy message
        self.output.value += "\n Order : "
        self.output.value += self.generate_order(report)

    def ticker_button_click(self, ticker, yahoo_api_key, td_api_key):
        report = gather_report(ticker=ticker.value,
                               yahoo_api=yahoo_api_key, td_api=td_api_key)

        self.ticker_output.value = json.dumps(report, indent=4)

    def generate_order(self, report):

        position = floor(report['position'])
        ticker = self.ticker.value
        buy_message = f'BUY +{position} {ticker} @{report["buy_price"]} LMT'
        sell_limit_message = f'SELL -{position} {ticker} @{report["sell_price"]} LMT GTC'
        stop_message = f'SELL -{position} {ticker} STP {report["stop_loss"]} GTC'

        message = f"\n{buy_message}\n{sell_limit_message}\n{stop_message}"

        return message
