## Instructions

1. setup python environment (venv, etc you know the deal)
2. activate env and pip install -r requirements.txt
3. jupyter notebook
4. Once notebook is open, open dashboard.ipynb, instantiate dashboard class.
