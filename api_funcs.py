from common import td_api_key, yahoo_api_key
import json
import requests
import math


def get_ticker_quotes(ticker, api_key):

    td_ameritrade_url = "https://api.tdameritrade.com/"
    market_data_quotes = f"v1/marketdata/{ticker}/quotes?"
    endpoint = td_ameritrade_url + market_data_quotes
    page = requests.get(url=endpoint,
                        params={'apikey': api_key}, verify=False)
    return page


def get_yahoo_ticker_quotes(ticker, api_key):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/auto-complete"
    querystring = {"q": ticker, "region": "US"}
    headers = {
        'x-rapidapi-key': api_key,
        'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
    }
    response = requests.request(
        "GET", url, headers=headers, params=querystring, verify=False)

    return response


def get_yahoo_ticker_statistics(ticker, api_key):
    url = "https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-statistics"

    querystring = {"symbol": ticker, "region": "US"}

    headers = {
        'x-rapidapi-key': api_key,
        'x-rapidapi-host': "apidojo-yahoo-finance-v1.p.rapidapi.com"
    }

    response = requests.request(
        "GET", url, headers=headers, params=querystring, verify=False)

    return response


def gather_report(ticker, yahoo_api, td_api):

    # gather info
    ticker_quote = get_ticker_quotes(ticker, td_api).json()
    yahoo_stats = get_yahoo_ticker_statistics(
        ticker=ticker, api_key=yahoo_api_key).json()
    volume = ticker_quote[ticker]['totalVolume']
    ticker_float = yahoo_stats['defaultKeyStatistics']['floatShares']['raw']

    # calculate float rotation
    float_rotation = volume / ticker_float
    analysis = "bearish" if float_rotation < 3 else "bullish"
    report = {
        ticker: {
            'float_rotation': float_rotation,
            'volume': (f"{volume:,d}"),
            'float': (f"{ticker_float:,d}"),
            'market_cap': (f"{yahoo_stats['summaryDetail']['marketCap']['raw']:,d}"),
            'analysis': analysis,
        }
    }
    return report


def calculate_position(stop_loss, buy_price, risk_amount):
    position = risk_amount / (buy_price - stop_loss)
    return position


def risk_reward_report(buy_price, sell_price, funds, stop_loss):

    position = math.floor(funds / buy_price)
    # position = calculate_position(stop_loss, buy_price, risk_amount)
    profit_amount = (position * sell_price) - (position * buy_price)
    two_to_one_sell_rec = ((buy_price - stop_loss) * 2) + buy_price
    stop_loss_risk = (buy_price * position) - (stop_loss * position)

    risk_reward_dict = {
        'position': position,
        'projected_profit': round(number=profit_amount, ndigits=2),
        'stop_loss_risk': round(number=stop_loss_risk, ndigits=2),
        'funds': funds,
        'funds_spent': buy_price * position,
        'buy_price': buy_price,
        'sell_price': sell_price,
        'recommended_sell': two_to_one_sell_rec,
        'stop_loss': stop_loss,
        'risk': 'good' if profit_amount >= stop_loss_risk * 2 else 'bad',
        'risk_ratio': f'{round(number = (profit_amount / stop_loss_risk),ndigits=2)} : 1',
    }

    return risk_reward_dict


if __name__ == "__main__":

    x = get_yahoo_ticker_statistics(
        ticker='FNKO', api_key="66dc1f51b4msh394fd6acabc633ap13de77jsna8d9f31acd6b")
