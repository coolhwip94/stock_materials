import json
import requests
from api_funcs import get_ticker_quotes, get_yahoo_ticker_quotes, get_yahoo_ticker_statistics
from common import td_api_key, yahoo_api_key


def gather_report(ticker, yahoo_api, td_api):

    # gather info
    ticker_quote = get_ticker_quotes(ticker, td_api).json()
    yahoo_stats = get_yahoo_ticker_statistics(
        ticker=ticker, api_key=yahoo_api_key).json()
    volume = ticker_quote[ticker]['totalVolume']
    ticker_float = yahoo_stats['defaultKeyStatistics']['floatShares']['raw']

    # calculate float rotation
    float_rotation = volume / ticker_float
    analysis = "bearish" if float_rotation < 3 else "bullish"
    report = {
        ticker: {
            'float_rotation': float_rotation,
            'volume': (f"{volume:,d}"),
            'float': (f"{ticker_float:,d}"),
            'market_cap': (f"{yahoo_stats['summaryDetail']['marketCap']['raw']:,d}"),
            'analysis': analysis,
        }
    }
    return report


def calculate_position(stop_loss, buy_price, risk_amount):
    position = risk_amount / (buy_price - stop_loss)
    return position


def risk_reward_report(buy_price, sell_price, risk_amount, stop_loss):

    position = calculate_position(stop_loss, buy_price, risk_amount)
    profit_amount = (position * sell_price) - (position * buy_price)
    two_to_one_sell_rec = ((buy_price - stop_loss) * 2) + buy_price
    money_needed = position * buy_price

    risk_reward_dict = {
        'position': position,
        'money_needed': money_needed,
        'projected_profit': profit_amount,
        'risk_amount': risk_amount,
        'buy_price': buy_price,
        'sell_price': sell_price,
        'recommended_sell': two_to_one_sell_rec,
        'stop_loss': stop_loss,
        'risk': 'good' if profit_amount >= risk_amount * 2 else 'bad',
    }

    return risk_reward_dict


if __name__ == "__main__":
    ticker = 'EVK'

    x = gather_report(ticker=ticker, yahoo_api=yahoo_api_key,
                      td_api=td_api_key)
    print(json.dumps(x, indent=4))

    risk_reward_query = {
        'buy_price': 1.55,
        'sell_price': 28.4,
        'stop_loss': 25.77,
        'risk_amount': 200,
    }

    y = risk_reward_report(**risk_reward_query)
    print(json.dumps(y, indent=4))
